This project uses rabbitmq to show changes in the remote directory

How to test

0 Run the project 

`docker-compose up --build`

1 Find out the server's container ID

`docker ps | grep server`

2 Log into server container

`docker exec -ti <ID> bash`

3 go to default watched directory (/tmp)

`cd /tmp`

4 Do something

`ls`

5 Check updates

  - Rabbitmq web interface to see messages (localhost:15677)

  - You can see messages in client's console
  
  Sample console output:
  
  ```
  client_1    | OPEN event: /tmp
  client_1    | ACCESS event: /tmp
  client_1    | CLOSE_NOWRITE event: /tmp
  ```

You can change the directory in `server/conf.json` file