import pyinotify
from producer import Producer
import json


class EventHandler(pyinotify.ProcessEvent):
    """Handling directory events"""
    def send_message(self, msg):
        """send msg to queue"""
        producer = Producer(queue="directory_events")
        producer.connect()
        producer.push(msg)
        producer.disconnect()

    def process_IN_ACCESS(self, event):
        print("ACCESS event: {}".format(event.pathname))
        self.send_message("ACCESS event: {}".format(event.pathname))

    def process_IN_ATTRIB(self, event):
        print("ATTRIB event: {}".format(event.pathname))
        self.send_message("ATTRIB event: {}".format(event.pathname))

    def process_IN_CLOSE_NOWRITE(self, event):
        self.send_message("CLOSE_NOWRITE event: {}".format(event.pathname))

    def process_IN_CLOSE_WRITE(self, event):
        print("CLOSE_WRITE event: {}".format(event.pathname))
        self.send_message("CLOSE_WRITE event: {}".format(event.pathname))

    def process_IN_CREATE(self, event):
        print("CREATE event: {}".format(event.pathname))
        self.send_message("CREATE event: {}".format(event.pathname))

    def process_IN_DELETE(self, event):
        print("DELETE event: {}".format(event.pathname))
        self.send_message("DELETE event: {}".format(event.pathname))

    def process_IN_MODIFY(self, event):
        print("MODIFY event: {}".format(event.pathname))
        self.send_message("MODIFY event: {}".format(event.pathname))

    def process_IN_OPEN(self, event):
        print("OPEN event: {}".format(event.pathname))
        self.send_message("OPEN event: {}".format(event.pathname))


def get_dir():
    """get directory to watch to"""
    with open('server/conf.json') as f:
        config = json.load(f)
        f.close()
    dir_ = config["dir"]
    print("Watching {} directory".format(dir_))
    return dir_


def main():
    # watch manager
    wm = pyinotify.WatchManager()
    dir_ = get_dir()
    wm.add_watch(dir_, pyinotify.ALL_EVENTS, rec=True)

    # event handler
    eh = EventHandler()

    # notifier
    notifier = pyinotify.Notifier(wm, eh)
    notifier.loop()


if __name__ == '__main__':
    print("Starting..")
    main()
