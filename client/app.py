from consumer import Consumer


def main():
    consumer_ = Consumer(queue="directory_events")
    consumer_.consume()


if __name__=="__main__":
    main()
